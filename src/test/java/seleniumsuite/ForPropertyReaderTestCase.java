package seleniumsuite;

import objects.User;
import org.testng.Assert;
import org.testng.annotations.Test;

import static services.Services.getTo;

/**
 * Created by rocky
 */
public class ForPropertyReaderTestCase extends BaseCase{
	@Test
	public void test_001(){
		//Создаем объект юзера из файла.
		User user = new User("properties/user.properties");
		getTo("https://templatemonster.com", driver);
		Assert.assertEquals(user.getEmail(),"qwerty@mail.cu");
	}
}
