package seleniumsuite;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static services.Log.error;
import static services.Log.info;
import static services.Services.takeScreenshot;

/**
 * Created by rocky.
 */
public class BaseCase {
	public WebDriver driver;
	public String testCaseName = this.getClass().toString().substring(this.getClass().toString().lastIndexOf(".") + 1);

	//Действия перед запуском именно сьюты в целом.
	@BeforeSuite
	public void startTestSuite(){
		info("=================================================================");
		info("Test suite started.");
		info("=================================================================");
	}

	//Действия для запуска непостредственно теста.
	@BeforeTest
	public void startTest() throws MalformedURLException {
		DesiredCapabilities capabilities;
		//Browser.
		capabilities = DesiredCapabilities.chrome();

		//Принимаем сертификаты (не обязательный параметр).
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		//Параметры для грида (хаба).
		String remoteDriver = "http://localhost:4444/wd/hub";

		driver = new RemoteWebDriver(new URL(remoteDriver), capabilities);
		info("Remote driver: "+remoteDriver);
		driver.manage().window().maximize();
		info("Maximize window.");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	}

	@AfterMethod
	public void screenshotOnTestFailure(ITestResult testResult) throws IOException {
		//Обрабатываем условие прохождение теста.
		if (testResult.getStatus() == ITestResult.FAILURE) {
			//Если тест упал запускаем скриншотилку.
			takeScreenshot(testCaseName, driver.getCurrentUrl(), driver);
			info("");
			error("TestCase: \"" + testCaseName + "\" FAILED.");
			info("");
		}
	}

	@AfterTest
	public void finishTest() {
		driver.quit();
		info("Close browser.");
		info("=================================================================");
		info("TestCase: \"" + testCaseName + "\" finished.");
		info("=================================================================");
		info("");
	}

	@AfterSuite
	public void finishTestSuite(){
		info("=================================================================");
		info("Test suite finished.");
		info("=================================================================");
	}
}
