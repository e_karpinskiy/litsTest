package functionalsuite;

import objects.GitHubUser;
import objects.User;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import services.Log;
import services.Services;

import static services.Services.getJsonGitHubUser;

/**
 * Created by rocky
 */
public class ApiTestCase{
	@Test
	public void test_003(){
		JSONObject jsonObject = getJsonGitHubUser("DavertMik");

		GitHubUser gitHubUser = new GitHubUser(jsonObject);

		Log.info(gitHubUser.getLogin());
		Log.info(gitHubUser.getLogin());
		Log.info(gitHubUser.getAvatar_url());
		Log.info(gitHubUser.getFollowers());

		//Checking.
		Assert.assertTrue(gitHubUser.getFollowers()>=240, "Userov stalo menshe.");
	}
}
