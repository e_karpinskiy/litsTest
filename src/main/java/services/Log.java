package services;

import org.apache.log4j.Logger;

/**
 * Created by rocky
 */
public class Log {
	//Создаем логгер из подключенной депенденси log4j.
	private static Logger Log = Logger.getLogger(services.Log.class.getName());

	//Задаем маски для логирования.
	public static <T extends Object> void info(T message) {
		Log.info(String.valueOf(message));
	}
	public static <T extends Object> void warn(T message) {
		Log.warn(String.valueOf(message));
	}
	public static <T extends Object> void error(T message) {
		Log.error(String.valueOf(message));
	}

}
