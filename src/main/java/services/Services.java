package services;

import org.apache.http.HttpResponse;
import org.json.JSONObject;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static services.Constants.GIT_HUB_API_USERS_URL;
import static services.HttpClient.getBody;
import static services.HttpClient.getResponse;
import static services.Log.error;
import static services.Log.info;
import static services.Log.warn;

/**
 * Created by rocky
 */
public abstract class Services {

	public static final String SCREENSHOTS_DIR = "target/screenshots/";

	public static void getTo(String url, WebDriver driver){
		driver.get(url);
		info("Get to "+url);
	}

	public static void takeScreenshot(String testCaseName, String message, WebDriver driver) {

		try {
			String screenshotName = testCaseName + "ScreenShot.png";
			final BufferedImage image;
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			image= ImageIO.read(scrFile);
			Graphics g = image.getGraphics();
			g.setFont(new Font("Arial Black", Font.BOLD, 20));
			g.setColor(Color.BLUE);
			g.drawString("URL: " + message, 50, 100);
			g.dispose();

			ImageIO.write(image, "png", new File(SCREENSHOTS_DIR + screenshotName));

			info("");
			warn("Screenshot captured.");
			warn("Screenshot name: \"" + screenshotName + "\".");
		}
		catch (WebDriverException | IOException e) {
			error("Catch " + e);
		}

	}

	public static JSONObject getJsonGitHubUser (String userName){
		HttpResponse httpResponse = getResponse(GIT_HUB_API_USERS_URL+userName);
		String body = getBody(httpResponse);
		return new JSONObject(body);
	}
}
