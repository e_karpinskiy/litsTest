package services;

/**
 * Created by rocky
 */
public class Constants {
	public static final String GIT_HUB_API_URL = "https://api.github.com/";
	public static final String GIT_HUB_API_USERS_URL = GIT_HUB_API_URL+"users/";
}
