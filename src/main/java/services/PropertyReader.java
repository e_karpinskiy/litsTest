package services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by rocky
 */
public class PropertyReader {
	//Объявляем поле с пакета java.util - Properties.
	public Properties properties;

	//Создаем метод, входным параметром которого является путь к нашему текстовому файлу.
	public PropertyReader(String file) {

		/**
		 * Здесь мы создаем экземпляр класса InputStream, который получает в качестве данных наш путь к файлу,
		 * тут может быть не только файл, но и другие данные, например массив или ссылка.
		 */
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(file);

		properties = new Properties();
			try {
				properties.load(inputStream);
			}
			catch (IOException e) {
				e.printStackTrace();
			}

	}

	//Создаем метод, который возвращает значение из файла проперти по ключу.
	public String getValue(String key){
		return properties.getProperty(key);
	}

}
