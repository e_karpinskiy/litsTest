package objects;

import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.json.JSONObject;

/**
 * Created by rocky
 */
@Data
public class GitHubUser {

	private String login;
	private int id;
	private int followers;
	private String avatar_url;

	public GitHubUser(JSONObject jsonObject) {
		login = jsonObject.getString("login");
		id = jsonObject.getInt("id");
		followers = jsonObject.getInt("followers");
		avatar_url = jsonObject.getString("avatar_url");
	}
}
