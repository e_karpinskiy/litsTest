package objects;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import services.PropertyReader;

/**
 * @author rocky
 */
@Data
public class User {

    private String id;
    private String emailUp;
    private String email;
    private String password;
    private String phone;

    //Создаем конструктор в параметрах которого путь к файлу.
    public User(String fileLocation) {
        //Тут и обявляеься проперти ридер.
        PropertyReader propertyReader = new PropertyReader(fileLocation);

        //Присваиваем полям объкта User значения из проперти ридера (из нашего файла).
        this.email = propertyReader.getValue("email");
        this.emailUp = propertyReader.getValue("emailUp");
        this.password = propertyReader.getValue("password");
        this.id = propertyReader.getValue("id");
        this.phone = propertyReader.getValue("phone");

        //Генерируем уникальный email по маске.
        String emailDef = propertyReader.getValue("emailUp");
        if (emailDef != null) {
            this.emailUp = emailDef.substring(0, emailDef.indexOf("@")) + "-"
                + String.valueOf(System.currentTimeMillis()) + emailDef.substring(emailDef.indexOf("@"));
        }
    }
}